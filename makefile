base64: main.cpp
	g++ main.cpp -o base64
run: base64
	./base64
clean:
	rm -rf base64 *.o *~ *.~
