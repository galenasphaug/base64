/** @file main.cpp
 Base64 encoding
 
 @author Galen Asphaug
 @version 1.0 12-24-17
 */

#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;
//input: string
//break into 24 bit (3 byte) senments
//for each 24 bits:
//split into 4 6-byte parts
//translate each 6 bytes into a character for b64

//if one (or more) of the 6-byte segments is empty, set all the bits to 0s and print an '='

// Base64 alphabet
const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890+/";
const char padding = '=';

bool get_bit(unsigned char byte, int position) {
  // Move bits left and get the left most bit
  // 0001000
  // 0010000
  //&1
  return (byte >> position) & 0x1;
}

// void print_bits(unsigned char & byte) {
//   for (int i = 0; i <= 7; i++) {
//     cout << get_bit(byte, i);
//   }
// }

// void print_bytes(char & str, int size) {
//   for (int i = 0; i < size; i++) {
//     cout << str[i];
//     print_bits(str[i]);
//     cout << ' ';
//   } cout << endl;
// }

//str must be at least 3 characters
bool * get_b64(char * str) {
  bool * a;
  a = new bool[3*8];
  
  
  for (unsigned i = 0; i < 3; i++) {
    for (unsigned j = 0; j < 8; j++) {
      a[i*j] = ((str[i] << j) & 0x1) > 0;
    }
    cout << endl;
  }
  
  return a;
}

int main() {
  char test [] = "M";
  //  char x = test[0];
  cout << get_b64(test)[1] << endl;
  //print_bytes(test, 3);
  
  vector<bool> data();
  
  
    
  // For each 3 characters
  // for (unsigned i = 0; i < data.length(); i+=3) {
  //     char segment[3] = {input[i], input[i+1], input[i+2]};
  
  //     //TODO: find a way to convert 24 bytes to 4 (6 byte) chars
  //     //Need to pad first two bits as 0s if storing as 8 bytes (char)
  
  //     // For each 6 bits
  //     for (int j = 0; j < 4; j++) {
  
  //         //cout << alphabet[/* 6 bytes as a char*/];
  //     }
  // }
  
  // cout << endl;
  return 0;
}
